import axios from "axios";
import qs from "qs";
import Cookies from "js-cookie";
import { Message } from "element-ui";
// import router from "../router";

const Axios = axios.create({
  baseURL: "/", // 因为我本地做了反向代理
  timeout: 10000,
  responseType: "json",
  withCredentials: true, // 是否允许带cookie这些
  headers: {
    "Content-Type": "application/x-www-form-urlencoded;charset=utf-8"
  }
});

//POST传参序列化(添加请求拦截器)
Axios.interceptors.request.use(
  config => {
    // 在发送请求之前做某件事
    if (config.method === "post") {
      // 序列化
      config.data = qs.stringify(config.data);
      // 温馨提示,若是贵公司的提交能直接接受json 格式,可以不用 qs 来序列化的
    }

    // 若是有做鉴权token , 就给头部带上token
    if (Cookies.get("token")) {
      config.headers.Authorization = Cookies.get("token");
    }
    return config;
  },
  error => {
    // error 的回调信息,看贵公司的定义
    Message({
      //  饿了么的消息弹窗组件,类似toast
      showClose: true,
      message: error && error.data.info.message,
      type: "error"
    });
    return Promise.reject(error.data.info.message);
  }
);

//返回状态判断(添加响应拦截器)
Axios.interceptors.response.use(
  res => {
    //对响应数据做些事
    if (res.data && res.data.status) {
      Message({
        //  饿了么的消息弹窗组件,类似toast
        showClose: true,
        message: res.data.info.message,
        type: "error"
      });
      return Promise.reject(res.data.info.message);
    }
    return res;
  },
  error => {
    return error;
  }
);

export function ajaxApi(method, url, params, queryObj) {
  return new Promise(function(resolve, reject) {
    const ajaxParams = { method, url };
    if (method === "post") {
      ajaxParams.headers = {
        "Content-type": "application/x-www-form-urlencoded"
      };
      ajaxParams.data = params;
      ajaxParams.params = queryObj;
    } else {
      ajaxParams.params = params;
    }
    Axios(ajaxParams)
      .then(res => {
        const result = JSON.parse(JSON.stringify(res.data));
        const code = Number(result.info.code);
        if (code === 10223) {
          resolve(result.data);
        } else if (
          [20014, 20015, 20016].includes(code) ||
          ["未查询到该用户"].includes(result.message)
        ) {
          // 未登录状态码
          console.log("未登录");
        } else {
          reject(result);
        }
      })
      .catch(() => {});
  });
}
// 对axios的实例重新封装成一个plugin ,方便 Vue.use(xxxx)
// export default {
//   install: function(Vue, Option) {
//     Object.defineProperty(Vue.prototype, "$http", { value: Axios });
//   }
// };
export default {
  api(method, url, params, queryObj) {
    return ajaxApi(method, url, params, queryObj);
  }
};
