import api from "@/fetch/api";

// 获取引用模板类型
export const fetchUseTempTypes = () => {
  return api.api("get", "/api/zncj/config/jzf/list");
};

export const fetchUseTempTypesPost = () => {
  return api.api("get", "/api/zncj/config/gjzn/list", { name: "wql" });
};
