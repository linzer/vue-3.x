import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import ElementUI from "element-ui";
import "element-ui/lib/theme-chalk/index.css";
import api from "@/fetch/api.js";
import "@/styles/reset.css";

if (process.env.NODE_ENV !== "production") {
  require("@/mock/index");
}

Vue.prototype.$api = api.api;

Vue.config.productionTip = false;

Vue.use(ElementUI);
new Vue({
  router,
  store,
  ElementUI,
  render: h => h(App)
}).$mount("#app");
