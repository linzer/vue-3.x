import * as testModule from "@/mock/modules/search.js";
import Mock from "mockjs";
mockDataCreate(testModule, true);

function mockDataCreate(mods, isOpen = true) {
  if (!isOpen) {
    return;
  }
  for (const key in mods) {
    const { url, method, data } = mods[key]();
    // console.log("\n");
    // console.log("url :", url);
    // console.log("method :", method);
    // console.log("data:", data);
    // console.log("\n");
    Mock.mock(url, method, data);
    Mock.mock(new RegExp(url, "g"), method, data);
  }
}
