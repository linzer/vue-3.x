// 菜单相关
// 轨迹分析菜单列表
export const trajAnalysisList = params => {
  const data = {
    data: [
      {
        icon: "algorithm",
        list: [
          {
            canConfig: 0,
            configType: 1,
            defaultZcj: "系数",
            explain: "人员涉稳情况的直观体现",
            id: "630C9798DF7C7B64E053E101A8C0E6AD",
            integral: 1,
            name: "系数",
            title: "基础算法",
            zfType: 0,
            zfremark: "人员的异常行为倾向分析"
          },
          {
            canConfig: 0,
            configType: 1,
            defaultZcj: "团伙分析",
            explain: "暂无说明",
            id: "630C9798DF7E7B64E053E101A8C0E6AD",
            integral: 1,
            name: "团伙分析概况",
            title: "基础算法",
            zfType: 0,
            zfremark: "团伙行为及关系成员分析"
          }
        ],
        title: "基础算法"
      },
      {
        icon: "personnel",
        list: [
          {
            canConfig: 0,
            configType: 0,
            defaultConfigInfo: [
              {
                fieldName: "qwcs",
                description: "超过多少次,整数",
                remark: "去往次数",
                type: "integer",
                value: 5
              },
              {
                fieldName: "mdd",
                description: "存储目的地名，如昆明",
                remark: "目的地",
                type: "array",
                value: ["浙江省/杭州市"]
              }
            ],
            defaultZcj: "时空规律",
            explain: "在两地之间频繁来往的异常行为",
            id: "60BD97E7DA066F70E053E101A8C0C11D",
            integral: 1,
            name: "人员频繁去往",
            title: "人员分析",
            userConfigInfo: [
              {
                fieldName: "qwcs",
                description: "超过多少次,整数",
                remark: "去往次数",
                type: "integer",
                value: 5
              },
              {
                fieldName: "mdd",
                description: "存储目的地名，如昆明",
                remark: "目的地",
                type: "array",
                value: ["浙江省/台州市", "浙江省/杭州市"]
              }
            ],
            zfType: 0,
            zfremark: "在两地之间频繁来往的异常行为"
          },
          {
            canConfig: 0,
            configType: 1,
            defaultZcj: "时空规律",
            explain: "曾去往某地但无返回轨迹的异常情况",
            id: "60BD97E7DA056F70E053E101A8C0C11D",
            integral: 1,
            name: "人员有去无回",
            title: "人员分析",
            zfType: 0,
            zfremark: "曾去往某地但无返回轨迹的异常情况"
          },
          {
            canConfig: 0,
            configType: 1,
            defaultZcj: "热点分析",
            explain: "对人员经常活动的区域和场所分析",
            id: "62CFF139288D2DA8E053E101A8C0AF8B",
            integral: 1,
            name: "人员落脚点",
            title: "人员分析",
            zfType: 0,
            zfremark: "对人员经常活动的区域和场所分析"
          },
          {
            canConfig: 0,
            configType: 1,
            defaultConfigInfo: [
              {
                fieldName: "qwcs",
                description: "超过多少次,整数",
                remark: "去往次数",
                type: "integer",
                value: 5
              },
              {
                fieldName: "mdd",
                description: "存储目的地名，如昆明",
                remark: "目的地",
                type: "array",
                value: ["浙江省/杭州市"]
              }
            ],
            defaultZcj: "热点分析",
            explain: "暂无说明",
            id: "62CFF139288E2DA8E053E101A8C0AF8B",
            integral: 1,
            name: "人员滞留点",
            title: "人员分析",
            zfType: 0,
            zfremark: "一段运动轨迹中的滞留状态分析"
          }
        ],
        title: "人员分析"
      },
      {
        icon: "vehicle",
        list: [
          {
            canConfig: 0,
            configType: 0,
            defaultConfigInfo: [
              {
                fieldName: "kklx",
                description: "存储卡口类型代码,数组类型",
                remark: "卡口类型",
                type: "array",
                value: ["出城卡口", "入城卡口", "火车站卡口"]
              },
              {
                fieldName: "jszq",
                description: "一周/一月",
                remark: "计算周期",
                type: "string",
                value: "周"
              },
              {
                fieldName: "btcxyz",
                description: "存储6:00,16:00,>=,10代表6点到16点，小于等于10天",
                remark: "白天出行阈值",
                type: "string",
                value: "6:00,16:00,<=,10"
              },
              {
                fieldName: "ywcxyz",
                description: "存储6:00,16:00,>=,10代表6点到16点，大于等于10天",
                remark: "夜晚出行阈值",
                type: "string",
                value: "16:00,20:00,>=,10"
              }
            ],
            defaultZcj: "时空规律",
            explain: "车辆白天出行较少夜间活动频繁的异常行为",
            id: "60BD97E7DA036F70E053E101A8C0C11D",
            integral: 1,
            name: "车辆昼伏夜出",
            title: "车辆分析",
            userConfigInfo: [
              {
                fieldName: "kklx",
                description: "存储卡口类型代码,数组类型",
                remark: "卡口类型",
                type: "array",
                value: ["出城卡口", "入城卡口", "火车站卡口"]
              },
              {
                fieldName: "jszq",
                description: "一周/一月",
                remark: "计算周期",
                type: "string",
                value: "周"
              },
              {
                fieldName: "btcxyz",
                description: "存储6:00,16:00,>=,10代表6点到16点，小于等于10天",
                remark: "白天出行阈值",
                type: "string",
                value: "6:00,16:00,<=,10"
              },
              {
                fieldName: "ywcxyz",
                description: "存储6:00,16:00,>=,10代表6点到16点，大于等于10天",
                remark: "夜晚出行阈值",
                type: "string",
                value: "16:00,20:00,>=,10"
              }
            ],
            zfType: 0,
            zfremark: "车辆白天出行较少夜间活动频繁的异常行为"
          }
        ],
        title: "车辆分析"
      },
      {
        icon: "mac",
        list: [
          {
            canConfig: 0,
            configType: 0,
            defaultConfigInfo: [
              {
                fieldName: "jmsc",
                description: "时长,类型(小时/分钟)",
                remark: "静默时长",
                type: "integer",
                value: "30,分钟"
              }
            ],
            defaultZcj: "时空规律",
            explain: "人员信号消失的异常行为",
            id: "60BD97E7DA006F70E053E101A8C0C11D",
            integral: 1,
            name: "MAC静默",
            title: "mac分析",
            userConfigInfo: [
              {
                fieldName: "jmsc",
                description: "时长,类型(小时/分钟)",
                remark: "静默时长",
                type: "integer",
                value: "15,分钟"
              }
            ],
            zfType: 0,
            zfremark: "人员信号消失的异常行为"
          },
          {
            canConfig: 0,
            configType: 0,
            defaultConfigInfo: [
              {
                fieldName: "lksc",
                description: "时长,类型(小时/分钟)",
                remark: "离开时长",
                type: "integer",
                value: "6,小时"
              }
            ],
            defaultZcj: "时空规律",
            explain: "人员离开所属管辖的异常行为",
            id: "60BD97E7D9FE6F70E053E101A8C0C11D",
            integral: 1,
            name: "MAC离开所属管辖",
            title: "mac分析",
            userConfigInfo: [
              {
                fieldName: "lksc",
                description: "时长,类型(小时/分钟)",
                remark: "离开时长",
                type: "integer",
                value: "16,小时"
              }
            ],
            zfType: 0,
            zfremark: "人员离开所属管辖的异常行为"
          },
          {
            canConfig: 0,
            configType: 1,
            defaultZcj: "时空规律",
            explain: "人员长时间停滞不动的异常行为",
            id: "60BD97E7DA016F70E053E101A8C0C11D",
            integral: 1,
            name: "MAC不移动",
            title: "mac分析",
            zfType: 0,
            zfremark: "人员长时间停滞不动的异常行为"
          },
          {
            canConfig: 0,
            configType: 0,
            defaultZcj: "时空规律",
            explain: "人员进入指定位置的异常行为",
            id: "60BD97E7D9FF6F70E053E101A8C0C11D",
            integral: 0,
            name: "MAC触网",
            title: "mac分析",
            zfType: 0,
            zfremark: "人员进入指定位置的异常行为"
          }
        ],
        title: "mac分析"
      }
    ],
    info: {
      code: 10223,
      message: "查询成功！"
    },
    status: 0
  };
  return {
    data: data,
    url: "/api/zncj/config/jzf/list",
    method: "get"
  };
};

// 轨迹智能菜单列表
export const trajIntelligenceList = params => {
  const data = {
    data: [
      {
        icon: "custom",
        list: [
          {
            createId: "6EA3DDC5E3470090C08D871FD829A23F",
            createTime: "2019-01-12 00:55:01",
            gjznZcjs: [
              {
                createId: "6EA3DDC5E3470090C08D871FD829A23F",
                gjznCj: {
                  $ref: "$.data[0].list[0]"
                },
                gjznZcjZfRels: [
                  {
                    configInfo: "[]",
                    gjznZcj: {
                      $ref: "$.data[0].list[0].gjznZcjs[0]"
                    },
                    gjznZf: {
                      canConfig: 0,
                      configType: 1,
                      defaultConfigInfo: [
                        {
                          fieldName: "qwcs",
                          description: "超过多少次,整数",
                          remark: "去往次数",
                          type: "integer",
                          value: 5
                        },
                        {
                          fieldName: "mdd",
                          description: "存储目的地名，如昆明",
                          remark: "目的地",
                          type: "array",
                          value: ["浙江省/杭州市"]
                        }
                      ],
                      defaultZcj: "热点分析",
                      explain: "暂无说明",
                      id: "62CFF139288E2DA8E053E101A8C0AF8B",
                      integral: 1,
                      name: "人员滞留点",
                      title: "人员分析",
                      zfType: 0,
                      zfremark: "一段运动轨迹中的滞留状态分析"
                    },
                    id: "ff808081684117bf0168414585c7000c",
                    integral: 1
                  },
                  {
                    configInfo: "[]",
                    gjznZcj: {
                      $ref: "$.data[0].list[0].gjznZcjs[0]"
                    },
                    gjznZf: {
                      canConfig: 0,
                      configType: 1,
                      defaultZcj: "热点分析",
                      explain: "对人员经常活动的区域和场所分析",
                      id: "62CFF139288D2DA8E053E101A8C0AF8B",
                      integral: 1,
                      name: "人员落脚点",
                      title: "人员分析",
                      zfType: 0,
                      zfremark: "对人员经常活动的区域和场所分析"
                    },
                    id: "ff808081684117bf0168414585c7000d",
                    integral: 1
                  }
                ],
                id: "ff808081684117bf0168414585c7000b",
                name: "热点分析",
                orderNum: 0
              },
              {
                createId: "6EA3DDC5E3470090C08D871FD829A23F",
                gjznCj: {
                  $ref: "$.data[0].list[0]"
                },
                gjznZcjZfRels: [
                  {
                    configInfo: "[]",
                    gjznZcj: {
                      $ref: "$.data[0].list[0].gjznZcjs[1]"
                    },
                    gjznZf: {
                      canConfig: 0,
                      configType: 0,
                      defaultConfigInfo: [
                        {
                          fieldName: "jmsc",
                          description: "时长,类型(小时/分钟)",
                          remark: "静默时长",
                          type: "integer",
                          value: "30,分钟"
                        }
                      ],
                      defaultZcj: "时空规律",
                      explain: "人员信号消失的异常行为",
                      id: "60BD97E7DA006F70E053E101A8C0C11D",
                      integral: 1,
                      name: "MAC静默",
                      title: "mac分析",
                      userConfigInfo: [
                        {
                          fieldName: "jmsc",
                          description: "时长,类型(小时/分钟)",
                          remark: "静默时长",
                          type: "integer",
                          value: "15,分钟"
                        }
                      ],
                      zfType: 0,
                      zfremark: "人员信号消失的异常行为"
                    },
                    id: "ff808081684117bf0168414585c20006",
                    integral: 1
                  },
                  {
                    configInfo: "[]",
                    gjznZcj: {
                      $ref: "$.data[0].list[0].gjznZcjs[1]"
                    },
                    gjznZf: {
                      canConfig: 0,
                      configType: 0,
                      defaultConfigInfo: [
                        {
                          fieldName: "lksc",
                          description: "时长,类型(小时/分钟)",
                          remark: "离开时长",
                          type: "integer",
                          value: "6,小时"
                        }
                      ],
                      defaultZcj: "时空规律",
                      explain: "人员离开所属管辖的异常行为",
                      id: "60BD97E7D9FE6F70E053E101A8C0C11D",
                      integral: 1,
                      name: "MAC离开所属管辖",
                      title: "mac分析",
                      userConfigInfo: [
                        {
                          fieldName: "lksc",
                          description: "时长,类型(小时/分钟)",
                          remark: "离开时长",
                          type: "integer",
                          value: "16,小时"
                        }
                      ],
                      zfType: 0,
                      zfremark: "人员离开所属管辖的异常行为"
                    },
                    id: "ff808081684117bf0168414585c20005",
                    integral: 1
                  },
                  {
                    configInfo: "[]",
                    gjznZcj: {
                      $ref: "$.data[0].list[0].gjznZcjs[1]"
                    },
                    gjznZf: {
                      canConfig: 0,
                      configType: 1,
                      defaultZcj: "时空规律",
                      explain: "人员长时间停滞不动的异常行为",
                      id: "60BD97E7DA016F70E053E101A8C0C11D",
                      integral: 1,
                      name: "MAC不移动",
                      title: "mac分析",
                      zfType: 0,
                      zfremark: "人员长时间停滞不动的异常行为"
                    },
                    id: "ff808081684117bf0168414585c2000a",
                    integral: 1
                  },
                  {
                    configInfo: "[]",
                    gjznZcj: {
                      $ref: "$.data[0].list[0].gjznZcjs[1]"
                    },
                    gjznZf: {
                      canConfig: 0,
                      configType: 0,
                      defaultConfigInfo: [
                        {
                          fieldName: "kklx",
                          description: "存储卡口类型代码,数组类型",
                          remark: "卡口类型",
                          type: "array",
                          value: ["出城卡口", "入城卡口", "火车站卡口"]
                        },
                        {
                          fieldName: "jszq",
                          description: "一周/一月",
                          remark: "计算周期",
                          type: "string",
                          value: "周"
                        },
                        {
                          fieldName: "btcxyz",
                          description:
                            "存储6:00,16:00,>=,10代表6点到16点，小于等于10天",
                          remark: "白天出行阈值",
                          type: "string",
                          value: "6:00,16:00,<=,10"
                        },
                        {
                          fieldName: "ywcxyz",
                          description:
                            "存储6:00,16:00,>=,10代表6点到16点，大于等于10天",
                          remark: "夜晚出行阈值",
                          type: "string",
                          value: "16:00,20:00,>=,10"
                        }
                      ],
                      defaultZcj: "时空规律",
                      explain: "车辆白天出行较少夜间活动频繁的异常行为",
                      id: "60BD97E7DA036F70E053E101A8C0C11D",
                      integral: 1,
                      name: "车辆昼伏夜出",
                      title: "车辆分析",
                      userConfigInfo: [
                        {
                          fieldName: "kklx",
                          description: "存储卡口类型代码,数组类型",
                          remark: "卡口类型",
                          type: "array",
                          value: ["出城卡口", "入城卡口", "火车站卡口"]
                        },
                        {
                          fieldName: "jszq",
                          description: "一周/一月",
                          remark: "计算周期",
                          type: "string",
                          value: "周"
                        },
                        {
                          fieldName: "btcxyz",
                          description:
                            "存储6:00,16:00,>=,10代表6点到16点，小于等于10天",
                          remark: "白天出行阈值",
                          type: "string",
                          value: "6:00,16:00,<=,10"
                        },
                        {
                          fieldName: "ywcxyz",
                          description:
                            "存储6:00,16:00,>=,10代表6点到16点，大于等于10天",
                          remark: "夜晚出行阈值",
                          type: "string",
                          value: "16:00,20:00,>=,10"
                        }
                      ],
                      zfType: 0,
                      zfremark: "车辆白天出行较少夜间活动频繁的异常行为"
                    },
                    id: "ff808081684117bf0168414585c20004",
                    integral: 1
                  },
                  {
                    configInfo: "[]",
                    gjznZcj: {
                      $ref: "$.data[0].list[0].gjznZcjs[1]"
                    },
                    gjznZf: {
                      canConfig: 0,
                      configType: 0,
                      defaultConfigInfo: [
                        {
                          fieldName: "qwcs",
                          description: "超过多少次,整数",
                          remark: "去往次数",
                          type: "integer",
                          value: 5
                        },
                        {
                          fieldName: "mdd",
                          description: "存储目的地名，如昆明",
                          remark: "目的地",
                          type: "array",
                          value: ["浙江省/杭州市"]
                        }
                      ],
                      defaultZcj: "时空规律",
                      explain: "在两地之间频繁来往的异常行为",
                      id: "60BD97E7DA066F70E053E101A8C0C11D",
                      integral: 1,
                      name: "人员频繁去往",
                      title: "人员分析",
                      userConfigInfo: [
                        {
                          fieldName: "qwcs",
                          description: "超过多少次,整数",
                          remark: "去往次数",
                          type: "integer",
                          value: 5
                        },
                        {
                          fieldName: "mdd",
                          description: "存储目的地名，如昆明",
                          remark: "目的地",
                          type: "array",
                          value: ["浙江省/台州市", "浙江省/杭州市"]
                        }
                      ],
                      zfType: 0,
                      zfremark: "在两地之间频繁来往的异常行为"
                    },
                    id: "ff808081684117bf0168414585c20008",
                    integral: 1
                  },
                  {
                    configInfo: "[]",
                    gjznZcj: {
                      $ref: "$.data[0].list[0].gjznZcjs[1]"
                    },
                    gjznZf: {
                      canConfig: 0,
                      configType: 0,
                      defaultZcj: "时空规律",
                      explain: "人员进入指定位置的异常行为",
                      id: "60BD97E7D9FF6F70E053E101A8C0C11D",
                      integral: 0,
                      name: "MAC触网",
                      title: "mac分析",
                      zfType: 0,
                      zfremark: "人员进入指定位置的异常行为"
                    },
                    id: "ff808081684117bf0168414585c20007",
                    integral: 0
                  },
                  {
                    configInfo: "[]",
                    gjznZcj: {
                      $ref: "$.data[0].list[0].gjznZcjs[1]"
                    },
                    gjznZf: {
                      canConfig: 0,
                      configType: 1,
                      defaultZcj: "时空规律",
                      explain: "曾去往某地但无返回轨迹的异常情况",
                      id: "60BD97E7DA056F70E053E101A8C0C11D",
                      integral: 1,
                      name: "人员有去无回",
                      title: "人员分析",
                      zfType: 0,
                      zfremark: "曾去往某地但无返回轨迹的异常情况"
                    },
                    id: "ff808081684117bf0168414585c20009",
                    integral: 1
                  }
                ],
                id: "ff808081684117bf0168414585c20003",
                name: "时空规律",
                orderNum: 0
              },
              {
                createId: "6EA3DDC5E3470090C08D871FD829A23F",
                gjznCj: {
                  $ref: "$.data[0].list[0]"
                },
                gjznZcjZfRels: [
                  {
                    configInfo: "[]",
                    gjznZcj: {
                      $ref: "$.data[0].list[0].gjznZcjs[2]"
                    },
                    gjznZf: {
                      canConfig: 0,
                      configType: 1,
                      defaultZcj: "系数",
                      explain: "人员涉稳情况的直观体现",
                      id: "630C9798DF7C7B64E053E101A8C0E6AD",
                      integral: 1,
                      name: "系数",
                      title: "基础算法",
                      zfType: 0,
                      zfremark: "人员的异常行为倾向分析"
                    },
                    id: "ff808081684117bf0168414585c80011",
                    integral: 1
                  }
                ],
                id: "ff808081684117bf0168414585c80010",
                name: "系数",
                orderNum: 0
              },
              {
                createId: "6EA3DDC5E3470090C08D871FD829A23F",
                gjznCj: {
                  $ref: "$.data[0].list[0]"
                },
                gjznZcjZfRels: [
                  {
                    configInfo: "[]",
                    gjznZcj: {
                      $ref: "$.data[0].list[0].gjznZcjs[3]"
                    },
                    gjznZf: {
                      canConfig: 0,
                      configType: 1,
                      defaultZcj: "团伙分析",
                      explain: "暂无说明",
                      id: "630C9798DF7E7B64E053E101A8C0E6AD",
                      integral: 1,
                      name: "团伙分析概况",
                      title: "基础算法",
                      zfType: 0,
                      zfremark: "团伙行为及关系成员分析"
                    },
                    id: "ff808081684117bf0168414585c8000f",
                    integral: 1
                  }
                ],
                id: "ff808081684117bf0168414585c7000e",
                name: "团伙分析",
                orderNum: 0
              }
            ],
            id: "ff808081684117bf0168414585c10002",
            name: "自定义场景测试别删"
          }
        ],
        title: "自定义场景"
      },
      {
        icon: "standard",
        list: [
          {
            createTime: "2018-09-27 16:17:27",
            gjznZcjs: [
              {
                createId: "ff8080816122098f016146bb5aba01b4",
                gjznCj: {
                  $ref: "$.data[1].list[0]"
                },
                gjznZcjZfRels: [
                  {
                    gjznZcj: {
                      $ref: "$.data[1].list[0].gjznZcjs[0]"
                    },
                    gjznZf: {
                      canConfig: 0,
                      configType: 0,
                      defaultConfigInfo: [
                        "BD2097E26BFA4FD68658E86B988A9ECE",
                        "CFB254A840C349CCB8339237C24816C4",
                        "CA8D970CF4ED4D4EB6E05DE10ED5901D",
                        "1C402F5909F143198D20FE78DF0CE8E7",
                        "8CFD575798374C8FB22D91FAA7338154",
                        "242499A338224A7884F9B16BA24D0067"
                      ],
                      defaultZcj: "行为规律",
                      explain: "复合标签",
                      id: "D412961519A74574B86504DA57820B24",
                      integral: 1,
                      name: "疑似涉毒",
                      title: "行为标签",
                      zfType: 2
                    },
                    id: "402890e665daf3520165db0b51600014",
                    integral: 1
                  }
                ],
                id: "402890e665daf3520165db0b51600015",
                name: "行为规律",
                orderNum: 0
              }
            ],
            id: "a949808d6619081501661a1aad3d001a",
            name: "涉毒场景",
            nav: "znfx/json/queryNav.json",
            remark: "具有涉毒倾向的行为分析",
            tjry: "yssd"
          },
          {
            createTime: "2018-09-26 19:54:08",
            gjznZcjs: [
              {
                createId: "ff8080816122098f016146bb5aba01b4",
                gjznCj: {
                  $ref: "$.data[1].list[1]"
                },
                gjznZcjZfRels: [],
                id: "4028904765e6de7a0165e6e4874b0001",
                name: "时空规律",
                orderNum: 0
              }
            ],
            id: "a93caead6615b108016615bab1360003",
            name: "前科人员静默场景",
            nav: "znfx/json/queryNav.json",
            remark: "前科人员信号消失的异常行为分析",
            tjry: "qkryjm"
          },
          {
            createTime: "2018-09-26 19:54:42",
            gjznZcjs: [
              {
                createId: "ff8080816122098f016146bb5aba01b4",
                gjznCj: {
                  $ref: "$.data[1].list[2]"
                },
                gjznZcjZfRels: [
                  {
                    gjznZcj: {
                      $ref: "$.data[1].list[2].gjznZcjs[0]"
                    },
                    gjznZf: {
                      canConfig: 0,
                      configType: 0,
                      defaultConfigInfo: [
                        "C55075166B0044BFA5379F724385E0E5",
                        "CA8D970CF4ED4D4EB6E05DE10ED5901D",
                        "1C402F5909F143198D20FE78DF0CE8E7",
                        "9008E7BA599E4A1C9FDAB928B7046B89"
                      ],
                      defaultZcj: "行为规律",
                      explain: "复合标签",
                      id: "96494A16DE52483892F725218056A52D",
                      integral: 1,
                      name: "人员异动",
                      title: "行为标签",
                      zfType: 2
                    },
                    id: "402890e665daf3520165db0ed48a001b",
                    integral: 1
                  }
                ],
                id: "402890e665daf3520165db0b51610019",
                name: "行为规律",
                orderNum: 0
              }
            ],
            id: "a93caead6615b108016615bb36c70006",
            name: "人员异动场景",
            nav: "znfx/json/queryNav.json",
            remark: "人员异动场景",
            tjry: "ryyd"
          },
          {
            gjznZcjs: [
              {
                createId: "10000",
                gjznCj: {
                  $ref: "$.data[1].list[3]"
                },
                gjznZcjZfRels: [
                  {
                    gjznZcj: {
                      $ref: "$.data[1].list[3].gjznZcjs[0]"
                    },
                    gjznZf: {
                      canConfig: 0,
                      configType: 0,
                      defaultZcj: "总体概况",
                      explain: "暂无说明",
                      id: "630C9798DF737B64E053E101A8C0E6AE",
                      integral: 1,
                      name: "总体概况",
                      title: "标准场景",
                      zfType: 0
                    },
                    id: "ff808081624290f201624291266a0000",
                    integral: 1
                  }
                ],
                id: "640B3F949A4C45FE9657711F7186A29E",
                name: "总体概况",
                orderNum: -1
              },
              {
                createId: "10000",
                gjznCj: {
                  $ref: "$.data[1].list[3]"
                },
                gjznZcjZfRels: [
                  {
                    gjznZcj: {
                      $ref: "$.data[1].list[3].gjznZcjs[1]"
                    },
                    gjznZf: {
                      $ref:
                        "$.data[0].list[0].gjznZcjs[0].gjznZcjZfRels[0].gjznZf"
                    },
                    id: "ff808081623c6ad401623c6b01d1000a",
                    integral: 1
                  },
                  {
                    gjznZcj: {
                      $ref: "$.data[1].list[3].gjznZcjs[1]"
                    },
                    gjznZf: {
                      $ref:
                        "$.data[0].list[0].gjznZcjs[0].gjznZcjZfRels[1].gjznZf"
                    },
                    id: "ff808081623c6ad401623c6b01d10009",
                    integral: 1
                  }
                ],
                id: "ADC73F0E9FDE4AF3B7CE29FD0CFF9022",
                name: "热点分析",
                orderNum: 0
              },
              {
                createId: "10000",
                gjznCj: {
                  $ref: "$.data[1].list[3]"
                },
                gjznZcjZfRels: [
                  {
                    gjznZcj: {
                      $ref: "$.data[1].list[3].gjznZcjs[2]"
                    },
                    gjznZf: {
                      canConfig: 0,
                      configType: 0,
                      defaultZcj: "人员特征",
                      explain: "暂无说明",
                      id: "FD04F62DA62A4D3092BEFB51288AC200",
                      integral: 1,
                      name: "群体上访",
                      title: "标准场景",
                      zfType: 0
                    },
                    id: "ff808081624278600162427891d30003",
                    integral: 1
                  },
                  {
                    gjznZcj: {
                      $ref: "$.data[1].list[3].gjznZcjs[2]"
                    },
                    gjznZf: {
                      canConfig: 0,
                      configType: 0,
                      defaultZcj: "人员特征",
                      explain: "暂无说明",
                      id: "E382D95EA9DF4B9F8AD20734828B72D1",
                      integral: 1,
                      name: "上访次数",
                      title: "标准场景",
                      zfType: 0
                    },
                    id: "ff808081624278600162427891d30002",
                    integral: 1
                  },
                  {
                    gjznZcj: {
                      $ref: "$.data[1].list[3].gjznZcjs[2]"
                    },
                    gjznZf: {
                      canConfig: 1,
                      configType: 0,
                      defaultZcj: "人员特征",
                      explain: "暂无说明",
                      id: "5F666E43D8F44B8DB512AE539AE39B59",
                      integral: 1,
                      name: "文化程度",
                      title: "标准场景",
                      zfType: 0
                    },
                    id: "ff808081624278600162427891d30004",
                    integral: 1
                  },
                  {
                    gjznZcj: {
                      $ref: "$.data[1].list[3].gjznZcjs[2]"
                    },
                    gjznZf: {
                      canConfig: 0,
                      configType: 0,
                      defaultZcj: "人员特征",
                      explain: "暂无说明",
                      id: "EC984BDF2B774D8D9E08711199FA9FED",
                      integral: 1,
                      name: "上访事由",
                      title: "标准场景",
                      zfType: 0
                    },
                    id: "ff808081624278600162427891d30001",
                    integral: 1
                  },
                  {
                    gjznZcj: {
                      $ref: "$.data[1].list[3].gjznZcjs[2]"
                    },
                    gjznZf: {
                      canConfig: 0,
                      configType: 0,
                      defaultZcj: "人员特征",
                      explain: "暂无说明",
                      id: "8A0CF182F834442C829D72931FABDBD6",
                      integral: 1,
                      name: "上访形式",
                      title: "标准场景",
                      zfType: 0
                    },
                    id: "ff808081624278600162427891d40005",
                    integral: 1
                  }
                ],
                id: "EEE9A639766E4F95B6592CE08194246D",
                name: "人员特征",
                orderNum: 0
              },
              {
                createId: "10000",
                gjznCj: {
                  $ref: "$.data[1].list[3]"
                },
                gjznZcjZfRels: [
                  {
                    gjznZcj: {
                      $ref: "$.data[1].list[3].gjznZcjs[3]"
                    },
                    gjznZf: {
                      $ref:
                        "$.data[0].list[0].gjznZcjs[2].gjznZcjZfRels[0].gjznZf"
                    },
                    id: "4028904765e63f0f0165e64172b60000",
                    integral: 1
                  }
                ],
                id: "40288934677355c70167735adfce000b",
                name: "系数",
                orderNum: 0
              },
              {
                createId: "10000",
                gjznCj: {
                  $ref: "$.data[1].list[3]"
                },
                gjznZcjZfRels: [],
                id: "a949808d6797dc3f01679b1e98a4003b",
                name: "时空规律",
                orderNum: 0
              },
              {
                createId: "10000",
                gjznCj: {
                  $ref: "$.data[1].list[3]"
                },
                gjznZcjZfRels: [
                  {
                    gjznZcj: {
                      $ref: "$.data[1].list[3].gjznZcjs[5]"
                    },
                    gjznZf: {
                      $ref:
                        "$.data[0].list[0].gjznZcjs[3].gjznZcjZfRels[0].gjznZf"
                    },
                    id: "ff808081623c6ad401623c6b01d00007",
                    integral: 1
                  }
                ],
                id: "A788298BC3AE47A7A99215A86A4F5A0A",
                name: "团伙分析",
                orderNum: 0
              }
            ],
            id: "1",
            name: "涉稳场景",
            nav: "znfx/json/queryNav.json",
            remark: "具有涉稳倾向的行为分析",
            tjry: "sw"
          }
        ],
        title: "标准场景"
      },
      {
        icon: "configure",
        list: [],
        title: "场景配置"
      }
    ],
    info: {
      code: 10223,
      message: "查询成功！"
    },
    status: 0
  };
  return {
    data: data,
    url: "/api/zncj/config/gjzn/list",
    method: "get"
  };
};
