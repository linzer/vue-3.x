module.exports = {
  // 基本路径
  publicPath: "/",
  // 输出文件目录
  outputDir: "dist",
  // eslint-loader 是否在保存的时候检查
  lintOnSave: false,
  runtimeCompiler: false,
  // webpack配置
  chainWebpack: () => {},
  configureWebpack: () => {},
  // vue-loader 配置项
  // 生产环境是否生成 sourceMap 文件
  productionSourceMap: false,
  // use thread-loader for babel & TS in production build
  // enabled by default if the machine has more than 1 cores
  parallel: require("os").cpus().length > 1,
  pwa: {},
  // webpack-dev-server 相关配置
  devServer: {
    //将服务启动后默认打开浏览器
    open: true,
    host: "0.0.0.0",
    port: 8080,
    https: false,
    hotOnly: false,
    proxy: {
      // 设置代理
      "/api": {
        target: "http://www.lzzyaf.com",
        changeOrigin: true,
        pathRewrite: {
          "^/api": "/"
        }
      }
    },
    before: () => {}
  },
  // 第三方插件配置
  pluginOptions: {
    // ...
  }
};
